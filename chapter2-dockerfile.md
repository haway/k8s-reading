
Docker 大小的最佳化
-------------------
縮小你的 Docker Image

利用 Dockerfile 建立應用程式 Docker
------------------------------------

Dockerfile:
FROM alpine
MAINTAINER haway <email>
COPY bin/kuard /kuard
ENTRYPOINT ["/kuard"]

alpine 是極小的 Linux 發行版本，只有 6MB 左右。

編譯 Docker
------------
 docker build -t kuard-amd64:1

標記你的 Docker
---------------
在推送到 Docker Registry 之前，你可以先標記一下你的 Docker Image

 docker tag kuard-amd64:1 gcr.io/kuar-demo/kuard-amd64:1

然後推送到 Registry

 docker push gcr.io/kuar-demo/kuard-amd64:1

執行 Docker
-----------

 docker run -d --name kuard --publish 8080:8080 gcr.io/kuar-demo/kuard-amd64:1

--publish 是將 docker 內的 port 對應到外部的 8080，可以用簡寫 -p

限制 Docker 使用系統資源
------------------------
記憶體

 docker run -d --name kuard -p 8080:8080 --memory 200m --memory-swap 1G gcr.io/kuar-demo/kuard-amd64:1

CPU

限制最多可使用兩個 CPU，實際上是會總共消耗共兩個 CPU 的資源，如果你有四個 CPU，則每個會消耗 50%。

 docker run -d --cpus=2 ....

限制固定只能使用某顆 CPU

 docker run -d --cpuset-cpus="1"
 docker run -d --cpuset-cpus="1,3"

 docker run -d --name kuard -p 8080:8080 --cpu-shares 1024 --memory 200m --memory-swap 1G gcr.io/kuar-demo/kuard-amd64:1

--cpu-shares 指的是這個 docker 跟其他 docker 所分配的權重，1024 是預設值。範例：

 $ docker run -it --rm --cpuset-cpus="0" --cpu-shares=512 u-stress:latest /bin/bash
 $ docker run -it --rm --cpuset-cpus="0" --cpu-shares=1024 u-stress:latest /bin/bash

則 container 第一個會分到 33% 的 CPU 權重，第二個會分到 66%

可以參考：https://www.cnblogs.com/sparkdev/p/8052522.html


