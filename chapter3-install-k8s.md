# 安裝 kubernetes

本範例使用 Debian 9 系統安裝 K8S

## 從 Source Code 安裝

根據[官方教學](https://kubernetes.io/docs/setup/release/building-from-source/)，先從 github 抓取 source code

    # git clone https://github.com/kubernetes/kubernetes.git
    # cd cd kubernetes/
    # make release
    
## 系統要求

1. 必須使用下列其中之一的作業系統：
 
- Ubuntu 16.04+
- Debian 9
- CentOS 7
- RHEL 7
- Fedora 25/26 (best-effort)
- HypriotOS v1.0.1+
- Container Linux (tested with 1800.6.0)


2. 每台機器都要有 2GB 以上的記憶體
3. 2 顆以上的 CPU
4. 叢集中的每台機器都要有完整的網路(public 或 private 都可以)
5. 每個 Node 都必須有唯一的 hostname、MAC address、product_uuid
6. 需要開啟防火牆的埠
7. **關閉 SWAP**

### 檢查 product_uuid 的方法：

檢查 product_uuid 的方法

    # cat /sys/class/dmi/id/product_uuid
    
## 開啟防火牆

開啟 Port 6443 與 10250，或是關閉防火牆：

    # systemctl stop iptables
    or
    # systemclt stop firewalld

## 安裝 kubelet、kubeadm 與 kubectl

### Debain 系統

指令來自[官方網站](https://kubernetes.io/docs/setup/independent/install-kubeadm/)：

    apt-get update && apt-get install -y apt-transport-https curl
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
    deb https://apt.kubernetes.io/ kubernetes-xenial main
    EOF
    apt-get update
    apt-get install -y kubelet kubeadm kubectl
    apt-mark hold kubelet kubeadm kubectl
    
### CentOS 系統

增加 yum 來源

    cat <<EOF > /etc/yum.repos.d/kubernetes.repo
    [kubernetes]
    name=Kubernetes
    baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
    enabled=1
    gpgcheck=1
    repo_gpgcheck=1
    gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
          https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
    EOF
    yum install -y kubelet kubeadm kubectl
    
    
## 啟動 k8s

啟動 kubelet

    # systemctl daemon-reload
    # systemctl restart kubelet
    
## 安裝完成

執行 kube 叢集初始化，如果有錯誤可以參考 錯誤 章節：

    kubeadm init
    
成功後會出現提示：

    Your Kubernetes master has initialized successfully!
    
    To start using your cluster, you need to run the following as a regular user:
    
      mkdir -p $HOME/.kube
      sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
      sudo chown $(id -u):$(id -g) $HOME/.kube/config
    
    You should now deploy a pod network to the cluster.
    Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
      https://kubernetes.io/docs/concepts/cluster-administration/addons/
    
    You can now join any number of machines by running the following on each node as root:
    
      kubeadm join 1.2.3.4:6443 --token 232v33.14hjmry1p7mwxkr0 --discovery-token-ca-cert-hash sha256:a07a158a6f38991b85aa8d130d73ebbcf5dbb422fe25f245fbe5b28380b11511


## 安裝網路 Flannel

設定 /proc/sys/net/bridge/bridge-nf-call-iptables = 1

    sysctl net.bridge.bridge-nf-call-iptables=1
    
套用 Flannel Pod

    kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml

## 檢查

取得 Node 資訊

    # kubectl get nodes
    # kubectl describe node (node-name)

## 其他

### 設定 CRI

如果你不是使用 docker 的話，要設定一下 ``/etc/default/kubelet`` 說明你的 CRI 種類。

### 使用 kubernetes 並開啟 SWAP

K8S 會要求你關閉主機的 SWAP 交換空間，原因是 K8S 希望 POD 在執行的時候能貼近完全使用 CPU/記憶體 100% 的狀況，在 CPU/記憶體不足的情況下，Unix 系統預設會將記憶體內的資料搬移到 SWAP 做交換，
但是這種交換動作對於容器平台來說非常浪費效能，K8S 希望在 CPU/記憶體滿載的時候直接將 Node 視為滿載，並回報 Master 調度。所以才會有將 SWAP 關閉，避免交換造成效能浪費。

詳細您可以參考[第二篇的回覆](https://serverfault.com/questions/881517/why-disable-swap-on-kubernetes)

但如果你跟筆者一樣是安裝在測試機器上面，而有其他程式需要使用 SWAP，你可以開啟 SWAP 後，修改 K8S 的設定，增加 ``--fail-swap-on=false`` 參數：

    # vi /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
    
    ....
    ExecStart=/usr/bin/kubelet --fail-swap-on=false $KUBELET_KUBECONFIG_ARGS

請直接把 ``--fail-swap-on`` 設定在 kubelet 命令後面，我嘗試增加 ``Environment`` 變數沒有用。


## 錯誤

### [ERROR NumCPU]: the number of available CPUs 1 is less than the required 2

最小需要 2 個 CPU 的限制，如果你是在 VirtualBox 等虛擬機安裝，可以改配兩個 vCPU，或是跳過錯誤：

    kubeadm init --ignore-preflight-errors=NumCPU,...
    
    
### [WARNING SystemVerification]: this Docker version is not on the list of validated versions: 17.05.0-ce. Latest validated version: 18.06

安裝新版的 docker

    https://docs.docker.com/install/linux/docker-ce/debian/#install-docker-ce-1
    
如果你的 Docker 已經是最新版，可以在 kubeadm init 時跳過錯誤

    kubeadm init --ignore-preflight-errors=SystemVerification,...

### [ERROR Swap]: running with swap on is not supported. Please disable swap

關閉 swap

    swapoff -a

記得修改 /etc/fstab 把 swap 拿掉

### [ERROR SystemVerification]: missing cgroups: memor

開啟 cgroup(Debian 預設關閉)，開啟 cgroup 後需要重新開機：

    # vim /etc/default/grub

    GRUB_CMDLINE_LINUX="cgroup_enable=memory"
    
    # update-group2
    # reboot
    
##### 文章資訊
slug:install-k8s  
title:安裝 kubernetes  
tags:Debian,install,k8s,Kubernetes
categories:k8s  
thumbnail:banner-1.jpg

