# Chapter 4

## Namespaces

Namespeac = 資料夾

管理不同的 pod
kubectl --namespace

    kubectl create namespace mystuff
    kubectl get namespace
    kubetl --namespace=mystuff ......
    kubectl namespace lsit

預設有的 namespaces:

    kube-pulic
    kube-system
    default

Run a yaml file will use default namespace "default".

View API Object via RESTful.

> https://xxxxx/xxxxx/../pods/my-pod

kubectl command can output josn or yaml file with options "-o"

    kubectl get pod my-pod -o [json|yaml]

    kubectl apply -f file.yaml

    kubectl delete -f file.yaml

    kubectl get pods (pod-name) -o jsonpath --template={.status.podIP}

view logs in pod

    kubectl logs (pod-name)
    kubectl logs (pod-name)  -f

Get into pod

    kubectl exec -it (pod-name) -- bash

CP file to muti 

Specied one contioner in Pod

    -c
    
##### 文章資訊
slug:k8s-namespaces  
title:K8S Namespace  
tags:k8s,Kubernetes,namespace
categories:k8s  
thumbnail:banner-1.jpg