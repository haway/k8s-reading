# Pod


去耦化 = 獨立化

Pod 是 K8S 內最小的佈署單位，這表示在一個 Pod 內的所有東西，都會佈署豆到同意台機器上。
Pod 中的每個容器，都運行在自己的 cgroup 中，但互相共享一些 Linux namespace。
同一個 Pod 共享 IP，Port 與主機名稱

## 我要怎麼規劃 Pod 裡面的東西


如果在不同的容器內，應用程式還能運作嗎？如果不能，那就放在同一個 Pod，反之，則放在不同的 Pod，且如果 Pod 在不同機器時，程式可以運作嗎？

## Pod Manifest 檔
 
- 主要作用就是定義 Pod
- 文字檔的格式
- 會儲存在 etcd 
- 定義了 Pod 的所所需資源與 K8S 設定，元件透過 API 取得設定後，就會知道要如何處理這個 Pod，譬如由調度器取得 Pod manifest 檔後，開始將 Pod 佈署到不同的 Node

## 建立 Pod

簡單建立 Pod

ubkubectl run kuard --image=gcr.io/kuar-demo/kuard-amd64:1

查看 Pod：

    kubectl get pods
    
刪除 Pod：

    kubectl delete deployments/kuard

刪除所有的 Pods：

    $ kubectl delete pods --all
    
## 建立 Pod Manifest 檔

支援 Yaml 或是 Json 格式撰寫，但通常使用 Yaml。

kuard-pod.yaml：

    apiVersion: v1
    kind: Pod
    metadata:
      name: kuard
    spec:
      containers:
      - image: gcr.io/kuar-demo/kuard-amd64:1
        name: kuard
        ports:
        - containerPort: 8080
          name: http
    protocol: TCP
    
執行 Pod：

    # kubectl apply -f kuard-pod.yaml
    
檢視 Pod 的詳細資訊：

    # kubectl describe pods kuard
    
移除 Pod

    # kubectl delete pods/kuard
    or
    # kubectl delete -f kuard-pod.yaml

並不會馬上被刪除，會等終止後才結束，避免已存在的連線中斷，預設的終止時間是 30 秒。

## 存取 Pod

### 設定連接埠轉發

    kubectl port-forward kuard 8080:8080
    
成功後就可以透過 http://localhost:8080 存取 kuard Pod。

### 取得日誌檔

    kubectl logs kuard
    
使用 -f 參數就可以持續監控 Pod。``--pervious`` 參數可以從容器的前一個執行體取得日誌，對於一直重不斷重新啟動 Pod 來說，這參數恨好用。

### 從容器複製檔案

如果你想從容器中複製 /root/config.txt 檔到本機，可以執行：

    kubectl cp <pod-name>:/root/config.txt ./config.txt
    
也可以複製本機檔案到 Pod 內：

    # kubectl cp /root/config.txt <pod-name>:/config.txt

複製檔案到 Pod 內，是應急的作法，因為你會再下次升級 Pod 的時候，這個檔案就遺失了。


## 健康檢查

K8S 內建健康檢查探測器有分 Liveness 與 Readiness 探測器

### Liveness 探測器

每一個容器定義一個探測器，寫在 Manifest 檔中，範例：

    apiVersion: v1
    kind: Pod
    metadata:
      name: kuard
    spec:
      containers:
        - image: gcr.io/kuar-demo/kuard-amd64:1
          name: kuard
          livenessProbe:
            httpGet:
              path: /healthy
              port: 8080
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 10
            failureThreshold: 3
          ports:
            - containerPort: 8080
              name: http
    protocol: TCP

此處使用 httpGet 探測器，針對 8080 ，在容器啟動後 5 秒後，開始測試，容器必須在 1 秒內有回應，HTTP 的狀態要大於 200 小於 400，每 10 秒測試一次，當連續三次失敗，則 K8S 會重新啟動此 Pod。

### Readiness 探測器

沒有講很多，補 G 一下

### tcpSocket 探測器

可以建立類似 tcp 的 socket 連線，建立完成就算成功，可以用來測試一些非 HTTP 的 API 或是資料庫。

### exec 探測器

利用容器內的腳本進行測試，K8S 可以利用 exec 直接執行命令稿，回傳 0 表示成功，非 0 表示失敗。

## 資源管理

### Request：所需最小資源

定義每個容器所需要的最小資源，當系統可以負荷的時候，會給予 100% 的資源，但是若 Pod 的數量過多，開始分配資源的時候，系統就會依照此設定來分配最小資源

    設定值是針對每個容器的最小需求，不是 Pod

## 資源管理

### Request：所需最小資源

定義每個容器所需要的最小資源，當系統可以負荷的時候，會給予 100% 的資源，但是若 Pod 的數量過多，開始分配資源的時候，系統就會依照此設定來分配最小資源

    設定值是針對每個容器的最小需求，不是 Pod

假設設定的是 0.5 的 CPU 需求，分配到 2 CPUs 的 Node 上，則此可以使用 2 CPUs，若是有新的 Pod 加入，則分配到 1 CPU，第三個進來則分配  0.66%，第四個進來則分配到 0.5 CPU，然後 Node 就會顯示滿載。
範例：

    apiVersion: v1
    kind: Pod
    metadata:
        name: kuard
    spec:
        containers:
        - name: kuard
        image: gcr.io/kuar-demo/kuard-amd64:1
        ports:
        - name: http
            containerPort: 8080
            protocol: TCP
        resources:
            requests:
                cpu: "500m"
                memory: "128Mi"

記憶體的限制跟 CPU 差不多，但是記憶體無法成重新分配，當記憶體不足時，使用量大的容器會被終止並重新啟動來降低記憶體的需求。

###  Limit：資源限制

Limit 就是容器所獲得的資源不會超過設定值，即使機器是屬於閒置的狀態，所分配到的資源還是會在 Limit 之下：

    apiVersion: v1
    kind: Pod
    metadata:
        name: kuard
    spec:
        containers:
        - name: kuard
        image: gcr.io/kuar-demo/kuard-amd64:1
        ports:
        - name: http
            containerPort: 8080
            protocol: TCP
        resources:
            requests:
                cpu: "500m"
                memory: "128Mi"
            limits:
                cpu: "1000m"
                memory: "256Mi"

                memory: "256Mi"

###  儲存空間(持久化資料)

容器內的資料在容器消失後，會一同消失，如果要保存容器內的資料，要將外部目錄掛在進容器內，目前的幾種方式

- 容器之間互相保持資料同步
- 掛載宿主主機的目錄到容器內，須避免 Node 一同損壞的問題
- 掛在 NFS 到容器內
- 掛載 AWS 等其他雲端的儲存空間到容器內

下面範例是掛載本機的 ``/var/lib/kuard`` 到容器內的 ``/data`` 內。

    apiVersion: v1
    kind: Pod
    metadata:
        name: kuard
    spec:
        containers:
        - name: kuard
        image: gcr.io/kuar-demo/kuard-amd64:1
        ports:
        - name: http
            containerPort: 8080
            protocol: TCP
        volumeMounts:
        - name: kuard-data
            mountPath: "/data"
        volumes:
        - name: kuard-data
        hostPath:
            path: "/var/lib/kuard"

## 總結

- Request 最小需求
- Limit 最大限制
- LivenessProbe 探針 (檢康檢查)
- ReadinessProbe 探針 (檢康檢查)
- 永久儲存區

範例：

    apiVersion: v1
    kind: Pod
    metadata:
    name: kuard
    spec:
    volumes:
        - name: "kuard-data"
        nfs:
            server: my.nfs.server.local
            path: "/exports"
    containers:
        - image: gcr.io/kuar-demo/kuard-amd64:1
        name: kuard
        ports:
            - containerPort: 8080
            name: http
            protocol: TCP
        resources:
            requests:
            cpu: "500m"
            memory: "128Mi"
            limits:
            cpu: "1000m"
            memory: "256Mi"
        volumeMounts:
            - mountPath: "/data"
            name: "kuard-data"
        livenessProbe:
            httpGet:
            path: /healthy
            port: 8080
            initialDelaySeconds: 5
            timeoutSeconds: 1
            periodSeconds: 10
            failureThreshold: 3
        readinessProbe:
            httpGet:
            path: /ready
            port: 8080
            initialDelaySeconds: 30
            timeoutSeconds: 1
            periodSeconds: 10
            failureThreshold: 3
            
##### 文章資訊
slug:k8s-pod  
title:k8s 的最小單位 Pod 
tags:pod,k8s,Kubernetes
categories:k8s  
thumbnail:banner-1.jpg