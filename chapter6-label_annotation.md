# Label 與 Annotation

## Label

標籤，用在 Pod 的身上。

### Label 語法

可以使用 / 的方式分隔主鍵的資料，如果有使用，則 / 之前必須使用 DNS 的格式，總長度不得超過  253 個字元，單一名稱不得超過 63 個字元，可以使用 ``-``、``_`` 與 ``.``。

範例：

主鍵 | 值
:---|:---
acme.com/app-version | 1.0.0
appVersion | 1.0.0
app.version | 1.0.0
kube.io/cluster-service | true

### 範例

建立一個有 ver=1,app=alpaca,env=prod 的 pod：

    $ kubectl run alpaca-prod --image=gcr.io/kuar-demo/kuard-amd64:1 --replicas=2 --labels="ver=1,app=alpaca,env=prod"

建立 ver=2,app=alpaca,env=test 的 pod：

    $ kubectl run alpaca-prod --image=gcr.io/kuar-demo/kuard-amd64:2 --replicas=2 --labels="ver=2,app=alpaca,env=test"

建立 ver=2,app=bandicoot,env=pord 的 pod：

    $ kubectl run alpaca-prod --image=gcr.io/kuar-demo/kuard-amd64:1 --replicas=2 --labels="ver=2,app=bandicoot,env=pord"

建立 ver=2,app=bandicoot,env=staing 的 pod：

    $ kubectl run alpaca-prod --image=gcr.io/kuar-demo/kuard-amd64:1 --replicas=2 --labels="ver=2,app=bandicoot,env=staing"

### 顯示 Labels

    $ kubectl get deployments --show-labels
    $ kubectl get deployments -L "(label-name)"
    $ kubectl get pods --show-labels

### 修改 Label

在建立 Label 標籤後，可以修改或刪除標籤

修改：

    $ kubectl label deployments alpaca-test "canary=true"

刪除，在標籤後面加一個 ``-``，就可以除標籤：

    $ kubectl label deployments alpaca-test "canary-"

### Label 篩選器

用 ``--selector`` 就可以篩選標籤

    $ kubectl get pods --selector="ver=2"
    $ kubectl get pods --selector="ver=2,app=bandicoot"
    $ kubectl get pods --selector="app in (bandicoot,alpaca)"

其他篩選運算子：

運算子  | 說明
:----|:----
key=value | key 等於 value
key!=value | key 不等於 value
ken in (val1,val2) | key 等於 val1 或是 val2
key notin (val1,val2) | key 不等於 val1 或是 val2
key | 有定義 key
!key | 沒有定義 key


    $ kubectl get pods --show-labels

幫所有的 Pod 加上標籤

    $ kubectl label pods --all status=abc

刪除所有
    $ kubectl label pods --all status-
    

## Annotation

讓 K8S 儲存中繼資料的空間，讓其他工具可以讀取的資料，像是 Json ，可以儲存在 Annotation 內，但是不能給選擇器使用。

可以想像 Annotation 是給其他工具的設定值(像是 etc 的作用，但不是真的 etcd)，其中也會記錄一些滾動升級的狀態。

Annotaion 的格式與 Label 一樣。


##### 文章資訊
slug:label_and_annotation  
title:Label 與 Annotation  
tags:label,annontation,k8s,Kubernetes
categories:k8s  
thumbnail:banner-1.jpg


