# Service

Service 就是實際要將 Pod 對外開放的時候，用來座內外網路對應的物件。

若沒有建立 Service 則 Pod 就只能內部通訊。

Service 是跨 Node 的，他會依照 SELECTOR 來抓取所有的 Pods。

不論是 Pod 重新啟動等等，外面進來的流量就是透過 Serivce 轉發到 Pod。

## 建立 Service

用 run 將 Pod 跑起來

    $ kubectl run alpaca-prod --image=gcr.io/kuar-demo/kuard-amd64:1 --replicas=3 --port=8000 --labels="ver=1,app=alpaca,env=pod"
    
在用 `expose` 建立 POD 的 Service 

    $ kubectl expose deployment alpaca-prod
    
## 查看 Service

用 get

    $ kubectl get services -o wide
    
## 將叢集對外開放

Service 預設會使用 `cluster IP` 的方式啟動，就只能內部連線，要將 Service 對外開放，要將網路類型改為 `NodePort`，除了 cluster IP 之外，系統會額外監聽一個連接埠(或是自己指定)。

設定好之後，只要任意連結到其中一個 Node 的 Port 都可以使用這個服務，你不需要知道 Pod 實際位於哪一個 Node 上。

先查看 Service

    $ kubectl describe service (service_name)
    
修改 sepc.type 為 `NodePort`

    $ kubectl edit service (service_name)
    ...
    spec:
      clusterIP: 1.2.3.4
      ports:
      - nodePort: 30366
        port: 8000
        protocol: TCP
        targetPort: 8000

## 手動使用 Service

手動模擬 Service 選取 Pods 的過程，先顯示所有 Pod 所帶的 Lables：

    $ kubectl get pods -o wide --show-labels
    
利用 `--selector` 來選取 Pods

    $ kubectl get pods -o wide --selector=app=alpaca,env=prod
    ....
    NAME
    xxxxxxxxx
    
Service 物件就是透過這樣的方式知道有哪些 Pod。

## kube-proxy 與 Cluster IP

Kube-proxy 物件會透過 API 查詢後端 Service 中的 Endpoint 的變化，如果有更動的時候，就會去更新 Cluster IP(Iptables) 的規則。



               +------------+
               | API Server |
               +------------+
                      |
                      |
    +-----------------|---------------+
    |  +--------+     V               |
    |  | Client |  +------------+     |    +---> Backend 1
    |  +--------+  | Kube-proxy |     |    |
    |      |       +------------+     |    |
    |      |              |           |    +---> Backend 2
    |      v              v           |    |
    |   +----------------------+      |    |
    |   |      Cluster IP      |-----------+---> Backend 3
    |   +----------------------+      |    |
    |                                 |    +---> Backend 4
    | Node                            |
    +---------------------------------+



# Endpoint

如果應用程式本身不需要 Cluster IP ，要直接使用 Service 的服務，就可以使用 Endpoint

    $ kubectl describe endpoints alpaca-prod
    

##### 文章資訊
slug:k8s-Service  
title:Service 介紹  
tags:service,k8s,Kubernetes  
categories:k8s  
thumbnail:banner-1.jpg