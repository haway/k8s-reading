import os
import sys
import demjson
import re
import requests
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods.posts import GetPosts, NewPost
from wordpress_xmlrpc.methods.users import GetUserInfo
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media, posts

#init
myweb = "https://okd.guide"
wp_api = myweb+"/wp-json/wp/v2"
debug = 1

img_list = {}
md_list = {}
html_list = {}
txt_list = {}

wp = Client( myweb+'/xmlrpc.php', 'walkphoto', 'Xsw#4rfC')
#print( wp.call( GetPosts() ) )
#print( wp.call( GetUserInfo() ) )

def check_img_id(pfile, p_name, p_ext):

	print(" in function check_img_id" );
	#https://wpgit.hdns.xyz/wp-json/wp/v2/media?slug=image1-jpg
	img_slug = p_name+"-"+p_ext[1:]

	print(" check via restful api: {}".format( wp_api+"/media?slug="+img_slug ) )
	r = requests.get( wp_api+"/media?slug="+img_slug )
	r_json = demjson.decode( r.content )
	if( len( r_json ) > 0  ):
		img_id = r_json[0]["id"]
		img_list[ pfile ] = img_id
		print( " Image Found, ID:{}".format( img_id ) )
		return img_id
	else:
		print( " Image not found, upload as a new picture" )
		return 0


def postimage(pfile, p_name, p_ext):
	
	if ( check_img_id( pfile, p_name, p_ext ) != 0 ):
		return img_list[ pfile ]

	print( " in function postimage" );

	if ( p_ext == '.jpg' ):
		img_type = 'image/jpeg'
	elif ( p_ext ==  '.png' ):
		img_type = 'image/png'
	elif ( p_ext == '.svg' ):
		img_type = 'image/svg'
	else:
		print( "Unknow image format: {}".format( pfile ) )
		sys.exit(0)

	print( " Get a new picture {}, it's {}.".format( pfile, img_type ) )

	data = {
		'name': pfile,
		'type': img_type,
		'overwrite': True
	}
	data['overwrite'] = True
	
	print( " Start upload..." )
	with open( pfile, 'rb' ) as img:
		data['bits'] = xmlrpc_client.Binary( img.read() )

	resp = wp.call( media.UploadFile( data ) )

	if ( resp['id'] ):
		img_list[ pfile ] = resp['id']
		print( " Upload image {} success.ID:{}, name:{}, url:{}".format( pfile, resp['id'], resp['file'], resp['url'] ) )
	else:
		print( " Upload image {} failed.".format( pfile ) )


def postmd(pfile, p_name, p_ext):
	#pfile = "test.md"

	print( " in function postmd, {}".format( pfile ) )
	f = open( pfile, "r" )
	all_content = f.read()

	regex = re.compile( r'slug:(.*)' )
	slug = regex.search( all_content ).group(1)
	regex = re.compile( r'tags:(.*)' )
	tags = regex.search( all_content ).group(1)
	regex = re.compile( r'title:(.*)' )
	title = regex.search( all_content ).group(1)
	regex = re.compile( r'categories:(.*)' )
	categories = regex.search( all_content ).group(1)
	regex = re.compile( r'thumbnail:(.*)' )
	img_thumbnail = regex.search( all_content ).group(1)
	img_pname, img_p_ext = os.path.splitext( img_thumbnail )
	check_img_id( img_thumbnail, img_pname, img_p_ext )
	img_thumbnail_id = img_list[ img_thumbnail ]

	print( " read done, title/slug/tags/categories/img_thumbnail/img_id: {}/{}/{}/{}/{}".format( title, slug, tags, categories, img_thumbnail, img_thumbnail_id ) )
	
	#slug="gandi-is-king"
	tags = tags.split(',')
	categories = categories.split(',')

	print( " check post slug via restful api: {}".format( wp_api+"/posts?slug="+slug ) )

	r = requests.get( wp_api+"/posts?slug="+slug )
	post = WordPressPost()
	# Default
	post.post_status = 'publish'
	post.content = all_content
	post.title = title
	post.slug = slug
	post.terms_names = {
		'post_tag': tags,
		'category': categories
	}

	#print( r.content )
	r_json = demjson.decode( r.content )
	#print( r_json )
	print( " response json lenght is: {}, 1 = exist".format( str( len(r_json) ) ) )

	if ( img_thumbnail_id != 0 ):
		post.thumbnail = img_thumbnail_id

	if ( ( img_thumbnail ) and ( img_thumbnail_id == 0 ) ):
		post.post_status = 'draft'

	if len(r_json) == 0:
		print( " New article" )
		print( wp.call(NewPost(post)) )
	else:
		aid = r_json[0]["id"]
		post.id = aid
		print( " Old article, post ID:{}".format( aid ) )
		print( wp.call(posts.EditPost( post.id, post )) )


if len(sys.argv) < 2:
	print("No argv")
	sys.exit(0)

pfiles = sys.argv[1: ]

for pfile in pfiles:

	print( "Get a file {}".format( pfile ) )
	pname, p_ext = os.path.splitext( pfile )

	if ( p_ext == '.png' ) or ( p_ext == '.jpg' ) or ( p_ext == '.svg' ):
		print( " +- {}, It's an image.".format( pfile ) )
		img_list[ pfile ] = 0
	elif ( p_ext == '.md' ):
		print( " +- {}, It's a markdown file.".format( pfile ) )
		md_list[ pfile ] = 0
	elif ( p_ext == '.html' ):
		print( " +- {}, It's a html file.".format( pfile ) )
		html_list[ pfile ] = 0
	elif ( p_ext == '.txt' ):
		print( " +- {}, It's a txt file.".format( pfile ) )
		txt_list[ pfile ] = 0
	else:
		print( "+- [Error] {} Unsupport file.".format( pfile ) )
		continue

for pfile in img_list:
	
	print( "Process Image: {} ".format( pfile ) )
	pname, p_ext = os.path.splitext( pfile )
	postimage( pfile, pname, p_ext )
	
for pfile in md_list:

	print( "Process MD: {} ".format( pfile ) )
	pname, p_ext = os.path.splitext( pfile )
	postmd( pfile, pname, p_ext )

for pfile in html_list:

	print( "Process HTML: {} ".format( pfile ) )
	pname, p_ext = os.path.splitext( pfile )
	postmd( pfile, pname, p_ext )

for pfile in txt_list:

	print( "Process Text: {} ".format( pfile ) )
	pname, p_ext = os.path.splitext( pfile )
	postmd( pfile, pname, p_ext )




